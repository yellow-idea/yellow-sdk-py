import datetime


def default_json(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()
