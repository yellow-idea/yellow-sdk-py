from pymongo import MongoClient
from .crud import get_created_at_iso


class MongoHelper:

    def __init__(self, ip="", user="", password="", db=""):
        self.ip = ip
        self.user = user
        self.password = password
        self.client = MongoClient(f"mongodb://{user}:{password}@{ip}:27017/{db}?authSource=admin")
        self.db = self.client[db]


class MongoDBCRUDRepository:

    def __init__(self, mongo_client, collection_name=""):
        self.collection = mongo_client[collection_name]
        self.collection_name = collection_name

    def list(self, filters, projection, pagination, is_id_to_str=False, is_convert_id=False):
        try:
            result = self.collection.find(filters, projection) \
                .skip(pagination['offset']) \
                .limit(pagination['limit'])
            total = self.collection.count_documents(filters)
            rows = []
            for item in result:
                if is_id_to_str is True:
                    item['_id'] = str(item['_id'])
                if is_convert_id is True:
                    item['id'] = item['_id']
                    item.pop('_id', None)
                rows.append(item)
            return {"rows": rows, "total": total}
        except Exception as e:
            print(f"[Error] {e}")
            return {"rows": [], "total": 0}

    def show(self, filters, projection=None, is_id_to_str=False, is_convert_id=False):
        try:
            result = self.collection.find_one(filters, projection)
            if is_id_to_str is True:
                result['_id'] = str(result['_id'])
            if is_convert_id is True:
                result['id'] = result['_id']
                result.pop('_id', None)
            return result
        except Exception as e:
            print(f"[Error] {e}")
            return None

    def create(self, payload):
        try:
            payload['created_at'] = get_created_at_iso()
            payload['updated_at'] = payload['created_at']
            payload['deleted_at'] = None
            result = self.collection.insert_one(payload)
            return result.inserted_id
        except Exception as e:
            print(f"[Error] {e}")
            return None

    def update(self, _id, payload):
        try:
            payload['updated_at'] = get_created_at_iso()
            result = self.collection.update_one({'_id': _id}, {'$set': payload})
            return result.modified_count
        except Exception as e:
            print(f"[Error] {e}")
            return None

    def delete(self, _id):
        try:
            result = self.collection.update_one({'_id': _id}, {'$set': {
                'deleted_at': get_created_at_iso()
            }})
            return result.modified_count
        except Exception as e:
            print(f"[Error] {e}")
            return None
