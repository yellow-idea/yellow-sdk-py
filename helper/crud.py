import uuid
from datetime import datetime, timedelta


def get_created_at():
    return (datetime.today() + timedelta(hours=7)).strftime("%Y-%m-%d %H:%M:%S")


def get_created_at_iso():
    # return (datetime.today() + timedelta(hours=7)).isoformat()
    return datetime.today().utcnow()


def convert_to_new_data(data):
    if 'id' not in data or data['id'] == '0' or data['id'] == 0:
        data['id'] = str(uuid.uuid1())
    d = get_created_at()
    if 'created_at' not in data:
        data['created_at'] = d
    data['updated_at'] = d
    return data


def convert_to_new_data_mongo(data):
    d = get_created_at()
    if 'created_at' in data:
        data['created_at'] = d
    data['updated_at'] = d
    return data
