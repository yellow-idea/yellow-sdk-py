from bson import ObjectId
from .mongo_repository import SegmentQuickRepository


class SegmentQuickService:

    def __init__(self, mongo_client):
        self.repository = SegmentQuickRepository(mongo_client)

    def list(self, payload):
        filters = {"deleted_at": None}
        if 'filter' in payload:
            if 'name' in payload['filter']:
                filters['name'] = {"$regex": f".*{payload['filter']['name']}.*"}
        return self.repository.list(
            filters=filters,
            projection=None,
            pagination={
                "limit": payload['limit'],
                "offset": payload['offset']
            },
            is_id_to_str=True,
            is_convert_id=True)

    def show(self, _id):
        if type(_id) is not ObjectId:
            _id = ObjectId(_id)
        return self.repository.show(
            filters={"_id": _id},
            projection=None,
            is_id_to_str=True,
            is_convert_id=True
        )

    def create(self, payload):
        _id = self.repository.create(payload)
        if _id is None:
            return {"code_return": -1, "message": "CREATE_ERROR"}
        return {"code_return": 1, "message": "CREATE_SUCCESS", "id": _id}

    def update(self, _id, payload):
        if type(_id) is not ObjectId:
            _id = ObjectId(_id)
        result = self.repository.update(_id, payload)
        if result is None:
            return {"code_return": -1, "message": "UPDATE_ERROR"}
        return {"code_return": 1, "message": "UPDATE_SUCCESS", "id": _id}

    def delete(self, _id):
        if type(_id) is not ObjectId:
            _id = ObjectId(_id)
        result = self.repository.delete(_id)
        if result is None:
            return {"code_return": -1, "message": "DELETE_ERROR"}
        return {"code_return": 1, "message": "DELETE_SUCCESS", "id": _id}