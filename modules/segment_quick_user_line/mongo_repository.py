from helper.mongo import MongoDBCRUDRepository


class SegmentQuickUserLineRepository:

    def __init__(self, mongo_client):
        crud = MongoDBCRUDRepository(mongo_client, "segment_quick_user_line")
        self.collection = crud.collection
        self.list = crud.list
        self.show = crud.show
        self.create = crud.create
        self.update = crud.update
        self.delete = crud.delete