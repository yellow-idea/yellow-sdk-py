from helper.mongo import MongoDBCRUDRepository


class CustomerRepository:

    def __init__(self, mongo_client):
        crud = MongoDBCRUDRepository(mongo_client, "customer")
        self.collection = crud.collection
        self.list = crud.list
        self.show = crud.show
        self.create = crud.create
        self.update = crud.update
        self.delete = crud.delete
