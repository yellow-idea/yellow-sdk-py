from helper.mongo import MongoDBCRUDRepository


class LineRichMenuRepository:

    def __init__(self, mongo_client):
        crud = MongoDBCRUDRepository(mongo_client, "line_rich_menu")
        self.collection = crud.collection
        self.list = crud.list
        self.show = crud.show
        self.create = crud.create
        self.update = crud.update
        self.delete = crud.delete
